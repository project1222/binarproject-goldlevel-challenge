package org.binar.ticket_reservation.service;

import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.Schedules;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScheduleService {

    // Save(create) Schedule By filmCode(Films' PK)
    ResponseEntity<Schedules> saveSchedule(Integer filmCode, String showDate, String startTime, String endTime, Integer ticketPrice);

    ResponseEntity<List<Schedules>> getSchedulesByFilmCode(Films filmCode);
}

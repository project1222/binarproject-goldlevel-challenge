package org.binar.ticket_reservation.service;

import org.binar.ticket_reservation.model.Films;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {

    // Save(create) operation
    ResponseEntity<Films> saveFilms(String filmName, Boolean isBeingShown);

    // Read operation
    ResponseEntity<List<Films>> showFilmsList();

    // Update operation
    ResponseEntity<Films> updateFilms(Integer filmCode, String filmName, Boolean isBeingShown);

    // Delete operation
    ResponseEntity<String> deleteFilmsById(Integer filmCode);

    ResponseEntity<List<Films>> showFilmsIsBeingShown();

    ResponseEntity<Films> getFilmsByFilmName(String filmName);
}

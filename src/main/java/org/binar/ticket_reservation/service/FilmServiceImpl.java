package org.binar.ticket_reservation.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.repository.FilmRepository;
import org.binar.ticket_reservation.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FilmServiceImpl implements FilmService{

    @Autowired
    @NonNull
    private FilmRepository filmRepository;

    @Autowired
    @NonNull
    private ScheduleRepository scheduleRepository;

    // Save(create) operation
    @Override
    public ResponseEntity<Films> saveFilms(String filmName, Boolean isBeingShown) {
        Films films = new Films(filmName, isBeingShown);
        return new ResponseEntity<>(filmRepository.save(films), HttpStatus.CREATED);
    }

    // Read operation
    @Cacheable(value = "showFilmsList")
    @Override
    public ResponseEntity<List<Films>> showFilmsList() {
        List<Films> filmList = filmRepository.findAll();
        if (filmList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(filmList, HttpStatus.OK);
    }

    // Update operation
    @Override
    public ResponseEntity<Films> updateFilms(Integer filmCode, String filmName, Boolean isBeingShown) {
        Films film = filmRepository.findById(filmCode).orElse(new Films());
        if (film.getFilmName() != null) {
            film.setFilmName(filmName);
            film.setIsBeingShown(isBeingShown);
            return new ResponseEntity<>(filmRepository.save(film), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Delete operation
    @Override
    public ResponseEntity<String> deleteFilmsById(Integer filmCode) {
        if (Boolean.TRUE.equals(filmRepository.existsById(filmCode))) {
            filmRepository.deleteById(filmCode);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Film with filmCode " + filmCode + " doesn't exist", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // Read Film is being shown
    @Cacheable(value = "showFilmsIsBeingShown")
    @Override
    public ResponseEntity<List<Films>> showFilmsIsBeingShown() {
        List<Films> filmList = filmRepository.findFilmByIsBeingShownIsTrue();
        if (filmList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(filmList, HttpStatus.OK);
    }

    @Cacheable(value = "getFilmsByFilmName")
    @Override
    public ResponseEntity<Films> getFilmsByFilmName(String filmName) {
        if (filmRepository.findByFilmName(filmName) != null) {
            return new ResponseEntity<>(filmRepository.findByFilmName(filmName), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}

package org.binar.ticket_reservation.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.Schedules;
import org.binar.ticket_reservation.repository.FilmRepository;
import org.binar.ticket_reservation.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ScheduleServiceImpl implements ScheduleService{

    @Autowired
    @NonNull
    private FilmRepository filmRepository;

    @Autowired
    @NonNull
    private ScheduleRepository scheduleRepository;

    // Save(create) Schedule By filmCode(Films' PK)
    @Override
    public ResponseEntity<Schedules> saveSchedule(Integer filmCode, String showDate, String startTime, String endTime, Integer ticketPrice) {
        if (filmRepository.findById(filmCode).isPresent()){
            Schedules schedule = new Schedules();
            Films films = filmRepository.findById(filmCode).orElse(null);
            schedule.setFilmCode(films);
            schedule.setShowDate(showDate);
            schedule.setStartTime(startTime);
            schedule.setEndTime(endTime);
            schedule.setTicketPrice(ticketPrice);

            return new ResponseEntity<>(scheduleRepository.save(schedule), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Cacheable(value = "getSchedulesByFilmCode")
    @Override
    public ResponseEntity<List<Schedules>> getSchedulesByFilmCode(Films filmCode) {
        if (scheduleRepository.findByFilmCode(filmCode).isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(scheduleRepository.findByFilmCode(filmCode), HttpStatus.OK);
    }

}

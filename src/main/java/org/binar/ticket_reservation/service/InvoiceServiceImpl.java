package org.binar.ticket_reservation.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.Schedules;
import org.binar.ticket_reservation.model.Seats;
import org.binar.ticket_reservation.model.Users;
import org.binar.ticket_reservation.repository.FilmRepository;
import org.binar.ticket_reservation.repository.ScheduleRepository;
import org.binar.ticket_reservation.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class InvoiceServiceImpl implements InvoiceService{

    @Autowired
    @NonNull
    private UsersRepository usersRepository;

    @Autowired
    @NonNull
    private FilmRepository filmRepository;

    @Autowired
    @NonNull
    private ScheduleRepository scheduleRepository;

    @Autowired
    @NonNull
    private UserService userService;

    @Autowired
    @NonNull
    private FilmService filmService;

    @Override
    public void generateInvoice(HttpServletResponse response, String username, String filmName,
                                Character studioName, Integer seatNumber) throws IOException, JRException, NullPointerException {

        JasperReport sourceFileName = JasperCompileManager
                .compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX
                        + "UserTicket.jrxml").getAbsolutePath());


        Users user = userService.getUsersByUsername(username).getBody();
        Films film = filmService.getFilmsByFilmName(filmName).getBody();
        Schedules schedule = scheduleRepository.findByFilmCode(film).get(0); // get the first schedule from list<Schedule>
        Seats seat = userService.addSeat(studioName, seatNumber).getBody();

        List<Map<String, String>> dataList = new ArrayList<>();
        Map<String, String> data = new HashMap<>();

        data.put("username", Objects.requireNonNull(user).getUsername());
        data.put("filmName", Objects.requireNonNull(film).getFilmName());
        data.put("showDate", schedule.getShowDate());
        data.put("showTime", schedule.getStartTime() + " - " + schedule.getEndTime());
        data.put("ticketPrice", "Rp" + schedule.getTicketPrice().toString() + ".000,00");
        data.put("seat", Objects.requireNonNull(seat).getSeatId().getStudioName().toString() + " "
                + seat.getSeatId().getSeatNumber().toString());
        dataList.add(data);

        // creating datasource from bean list
        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "Anas Syahirul A");
        JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, beanColDataSource);

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; filename=UserTicket.pdf;");

        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }
}

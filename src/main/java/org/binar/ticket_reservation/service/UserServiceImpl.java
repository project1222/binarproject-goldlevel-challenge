package org.binar.ticket_reservation.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.binar.ticket_reservation.model.SeatId;
import org.binar.ticket_reservation.model.Seats;
import org.binar.ticket_reservation.model.Users;
import org.binar.ticket_reservation.repository.SeatRepository;
import org.binar.ticket_reservation.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{

    @Autowired
    @NonNull
    private UsersRepository usersRepository;

    @Autowired
    @NonNull
    private SeatRepository seatRepository;

    // Save(create) operation
    @Override
    public ResponseEntity<Users> saveUser(String username, String emailAddress, String password) {
        Users user = new Users();
        user.setUsername(username);
        user.setEmailAddress(emailAddress);
        user.setPassword(password);
        return new ResponseEntity<>(usersRepository.save(user), HttpStatus.CREATED);
    }

    // Read operation
    @Cacheable(value = "showUsersList")
    @Override
    public ResponseEntity<List<Users>> showUsersList() {
        List<Users> userList = usersRepository.findAll();
        if (userList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    // Update operation
    @Override
    public ResponseEntity<Users> updateUser(Integer userId, String username, String emailAddress, String password) {
        Users user = usersRepository.findById(userId).orElse(new Users());
        if (user.getUsername() != null) {
            user.setUsername(username);
            user.setEmailAddress(emailAddress);
            user.setPassword(password);
            return new ResponseEntity<>(usersRepository.save(user), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<String> deleteUserById(Integer userId) {

        if (Boolean.TRUE.equals(usersRepository.existsByUserId(userId))) {
            usersRepository.deleteById(userId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("user with userId " + userId + " doesn't exist", HttpStatus.INTERNAL_SERVER_ERROR);//

    }

    @Cacheable(value = "getUsersByUsername")
    @Override
    public ResponseEntity<Users> getUsersByUsername(String username) {
        return new ResponseEntity<>(usersRepository.findByUsername(username), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Seats> addSeat(Character studioName, Integer seatNumber) {
        Seats seat = new Seats(new SeatId(studioName, seatNumber));
        return new ResponseEntity<>(seatRepository.save(seat), HttpStatus.CREATED);
    }

}

package org.binar.ticket_reservation.service;

import net.sf.jasperreports.engine.JRException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public interface InvoiceService {
    public void generateInvoice(HttpServletResponse response, String username, String filmName,
                                Character studioName, Integer seatNumber) throws IOException, JRException;
}

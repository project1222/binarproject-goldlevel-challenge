package org.binar.ticket_reservation.service;

import org.binar.ticket_reservation.model.Seats;
import org.binar.ticket_reservation.model.Users;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    // Save(create) operation
    ResponseEntity<Users> saveUser(String username, String emailAddress, String password);

    // Read operation
    ResponseEntity<List<Users>> showUsersList();

    // Update operation
    ResponseEntity<Users> updateUser(Integer userId, String username, String emailAddress, String password);

    // Delete operation
    ResponseEntity<String> deleteUserById(Integer userId);

    // Read operation
    ResponseEntity<Users> getUsersByUsername(String username);

    ResponseEntity<Seats> addSeat(Character studioName, Integer seatNumber);

}

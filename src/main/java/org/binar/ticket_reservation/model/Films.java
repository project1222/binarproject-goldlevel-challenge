package org.binar.ticket_reservation.model;

import org.binar.ticket_reservation.dto.FilmDto;

import javax.persistence.*;

@Entity(name = "films")
public class Films {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "film_code")
    private Integer filmCode;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "is_being_shown")
    private Boolean isBeingShown;

    public Films() {

    }

    public Films(FilmDto filmDto) {
        this.filmName = filmDto.getFilmName();
        this.isBeingShown = filmDto.getBeingShown();
    }

    public Films(Integer filmCode) {
        this.filmCode = filmCode;
    }

    public Films(String filmName, Boolean isBeingShown) {
        this.filmName = filmName;
        this.isBeingShown = isBeingShown;
    }

    public Films(Integer filmCode, String filmName, Boolean isBeingShown) {
        this.filmCode = filmCode;
        this.filmName = filmName;
        this.isBeingShown = isBeingShown;
    }

    public Integer getFilmCode() {
        return filmCode;
    }

    public void setFilmCode(Integer filmCode) {
        this.filmCode = filmCode;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public Boolean getIsBeingShown() {
        return isBeingShown;
    }

    public void setIsBeingShown(Boolean isBeingShown) {
        this.isBeingShown = isBeingShown;
    }

}

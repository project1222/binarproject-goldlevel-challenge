package org.binar.ticket_reservation.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity(name = "seats")
public class Seats {

    @EmbeddedId
    private SeatId seatId;

    public Seats() {

    }

    public Seats(SeatId seatId) {
        this.seatId = seatId;
    }

    public SeatId getSeatId() {
        return seatId;
    }

    public void setSeatId(SeatId seatId) {
        this.seatId = seatId;
    }

}

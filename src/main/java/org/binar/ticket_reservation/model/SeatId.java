package org.binar.ticket_reservation.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SeatId implements Serializable {

    @Column(name = "studio_name")
    private Character studioName;

    @Column(name = "seat_number")
    private Integer seatNumber;

    public SeatId() {

    }

    public SeatId(Character studioName, Integer seatNumber) {
        this.studioName = studioName;
        this.seatNumber = seatNumber;
    }

    public Character getStudioName() {
        return studioName;
    }

    public void setStudioName(Character studioName) {
        this.studioName = studioName;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

}

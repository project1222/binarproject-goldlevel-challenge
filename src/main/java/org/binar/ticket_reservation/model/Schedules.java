package org.binar.ticket_reservation.model;

import org.binar.ticket_reservation.dto.ScheduleDto;

import javax.persistence.*;

@Entity(name = "schedules")
public class Schedules {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    private Integer scheduleId;

    @ManyToOne(targetEntity = Films.class)
    @JoinColumn(name = "film_code")
    private Films filmCode;

    @Column(name = "show_date")
    private String showDate;

    @Column(name = "start_time")
    private String startTime;

    @Column(name = "end_time")
    private String endTime;

    @Column(name = "ticket_price")
    private Integer ticketPrice;

    public Schedules() {

    }

    public Schedules(ScheduleDto scheduleDto) {
        this.filmCode = scheduleDto.getFilmCode();
        this.showDate = scheduleDto.getShowDate();
        this.startTime = scheduleDto.getStartTime();
        this.endTime = scheduleDto.getEndTime();
        this.ticketPrice = scheduleDto.getTicketPrice();
    }

    public Schedules(Integer scheduleId, Films filmCode, String showDate, String startTime, String endTime, Integer ticketPrice) {
        this.scheduleId = scheduleId;
        this.filmCode = filmCode;
        this.showDate = showDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.ticketPrice = ticketPrice;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Films getFilmCode() {
        return filmCode;
    }

    public void setFilmCode(Films filmCode) {
        this.filmCode = filmCode;
    }

    public String getShowDate() {
        return showDate;
    }

    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(Integer ticketPrice) {
        this.ticketPrice = ticketPrice;
    }
}

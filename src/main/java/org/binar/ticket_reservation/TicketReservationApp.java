package org.binar.ticket_reservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketReservationApp {

	public static void main(String[] args) {
		SpringApplication.run(TicketReservationApp.class, args);
	}

}

package org.binar.ticket_reservation.repository;

import org.binar.ticket_reservation.enumeration.ERole;
import org.binar.ticket_reservation.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Integer> {
    Optional<Roles> findByName(ERole name);
}

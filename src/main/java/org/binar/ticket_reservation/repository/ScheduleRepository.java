package org.binar.ticket_reservation.repository;

import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ScheduleRepository extends JpaRepository<Schedules, Integer> {
    @Modifying
    @Query(nativeQuery = true, value = "delete from schedules where film_code= :film_code")
    void deleteScheduleFilm(
            @Param("film_code") Integer filmCode
    );

    List<Schedules> findByFilmCode(Films filmCode);

}

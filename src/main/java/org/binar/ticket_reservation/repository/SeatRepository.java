package org.binar.ticket_reservation.repository;

import org.binar.ticket_reservation.model.SeatId;
import org.binar.ticket_reservation.model.Seats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface SeatRepository extends JpaRepository<Seats, SeatId> {

    @Modifying
    @Query(nativeQuery = true, value = "insert into seats(studio_name, seat_number) values(:studioName, :seatNumber)")
    ResponseEntity<Seats> addSeats(
            @Param("studioName") Character studioName,
            @Param("seatNumber") String seatNumber
    );

}

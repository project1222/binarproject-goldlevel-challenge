package org.binar.ticket_reservation.repository;

import org.binar.ticket_reservation.model.Films;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface FilmRepository extends JpaRepository<Films, Integer> {

    @Query("select f from films f where is_being_shown = true")
    public List<Films> findFilmByIsBeingShownIsTrue();

    @Query(nativeQuery = true, value = "select f from films as f where film_code = :film_code")
    Films findFilmbyFilmCode();

    public Films findByFilmName(String filmName);

}

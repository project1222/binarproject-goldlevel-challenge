package org.binar.ticket_reservation.scheduler;

import org.binar.ticket_reservation.service.FilmService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Objects;

public class FilmJob extends QuartzJobBean {
    private static final Logger LOG = LoggerFactory.getLogger(FilmJob.class);

    @Autowired
    private FilmService filmService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

        Objects.requireNonNull(filmService.showFilmsIsBeingShown().getBody())
                .forEach(films -> LOG.info(films.getFilmName()));

    }
}

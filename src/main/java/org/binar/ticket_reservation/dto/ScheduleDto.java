package org.binar.ticket_reservation.dto;

import org.binar.ticket_reservation.model.Films;

public class ScheduleDto {
    private Films filmCode;
    private String showDate;
    private String startTime;
    private String endTime;
    private Integer ticketPrice;

    public ScheduleDto() {

    }

    public ScheduleDto(Films filmCode, String showDate, String startTime, String endTime, Integer ticketPrice) {
        this.filmCode = filmCode;
        this.showDate = showDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.ticketPrice = ticketPrice;
    }

    public Films getFilmCode() {
        return filmCode;
    }

    public String getShowDate() {
        return showDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public Integer getTicketPrice() {
        return ticketPrice;
    }
}

package org.binar.ticket_reservation.dto;

public class FilmDto {
    private String filmName;
    private Boolean isBeingShown;

    public String getFilmName() {
        return filmName;
    }

    public Boolean getBeingShown() {
        return isBeingShown;
    }
}

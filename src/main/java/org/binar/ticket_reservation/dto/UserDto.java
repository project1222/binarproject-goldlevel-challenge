package org.binar.ticket_reservation.dto;

public class UserDto {
    private String username;
    private String emailAddress;
    private String password;

    public String getUsername() {
        return username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }
}

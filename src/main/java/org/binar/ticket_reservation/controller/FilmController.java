package org.binar.ticket_reservation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.binar.ticket_reservation.dto.FilmDto;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.List;

@RequiredArgsConstructor
@Tag(name = "Films", description = "API for processing with Films entity")
@RestController
@RequestMapping("/api")
public class FilmController {

    @Autowired
    @NonNull
    private FilmService filmService;

//    **********************************   Admin Access   **********************************

    @Operation(summary = "Add a new film to Films Entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "new film successfully added",
                    content = {@Content(schema = @Schema(example = "{" +
                            "\"filmCode\": " + "1, " +
                            "\"filmName\": " + "\"Ice Age\", " +
                            "\"isBeingShown\": " + "true" +
                            "}") )})
    })
    @PostMapping("/admin/film")
    public ResponseEntity<Films> addFilm(@RequestBody FilmDto filmDto) {
        Films films = new Films(filmDto);
        return filmService.saveFilms(films.getFilmName(), films.getIsBeingShown());
    }

    @Operation(summary = "Update a film information by its Code/Primary Key")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The film with certain filmCode has been updated",
                    content = {@Content(schema = @Schema(example = "{" +
                            "\"filmCode\": " + "2, " +
                            "\"filmName\": " + "\"Ice Age\", " +
                            "\"isBeingShown\": " + "true" +
                            "}") )}),
            @ApiResponse(responseCode = "404", description = "The film with certain filmCode doesnt exist",
                    content = {@Content()})
    })
    @PutMapping("/admin/film/{filmCode}")
    public ResponseEntity<Films> updateFilm(@Parameter(description = "code of film to be searched")
                                                @PathVariable("filmCode") Integer filmCode,
                                            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "A JSON object containing film information")
                                                @RequestBody FilmDto filmDto) {
        Films film = new Films(filmDto);
        return filmService.updateFilms(filmCode, film.getFilmName(), film.getIsBeingShown());
    }

    @Operation(summary = "Delete a film by its Code/Primary Key")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "The film successfully deleted",
                    content = {@Content(schema = @Schema(example = "Film with certain filmCode has been deleted"))}),
            @ApiResponse(responseCode = "500", description = "No film found",
                    content = {@Content(schema = @Schema(example = "Film with certain filmCode doesn't exist") )})
    })
    @DeleteMapping("/admin/film/{filmCode}")
    public ResponseEntity<String> deleteFilm(@Parameter(description = "code of film to be searched")
                                             @PathVariable Integer filmCode) {
        return filmService.deleteFilmsById(filmCode);
    }

//    **********************************   Public Access   **********************************

    @Operation(summary = "Get all currently showing film")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of currently showing film",
                    content = {@Content(schema = @Schema(example = "[\n{" +
                            "\"filmCode\": " + "2, " +
                            "\"filmName\": " + "\"Ice Age\", " +
                            "\"isBeingShown\": " + "true" +
                            "}, \n" +
                            "{" +
                            "\"filmCode\": " + "3, " +
                            "\"filmName\": " + "\"Mortal Combat \", " +
                            "\"isBeingShown\": " + "true" +
                            "}\n]") )}),
            @ApiResponse(responseCode = "204", description = "There is no currently showing film",
                    content = {@Content()})
    })
    @GetMapping("/public/isBeingShown/films")
    public ResponseEntity<List<Films>> readFilmIsBeingShown() {
        return filmService.showFilmsIsBeingShown();
    }


//    ******************************************************************************************************

    @PermitAll
    @Operation(summary = "Get all film in Films Entity")
    @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "List of the Films",
            content = {@Content(schema = @Schema(example = "[\n{" +
                    "\"filmCode\": " + "2, " +
                    "\"filmName\": " + "\"Ice Age\", " +
                    "\"isBeingShown\": " + "true" +
                    "}, \n" +
                    "{" +
                    "\"filmCode\": " + "3, " +
                    "\"filmName\": " + "\"Ice Age 2\", " +
                    "\"isBeingShown\": " + "false" +
                    "}\n]") )}),
    @ApiResponse(responseCode = "204", description = "There is no film in database",
            content = {@Content()})
    })
    @GetMapping("/films")
    public ResponseEntity<List<Films>> readAllFilm() {
    return filmService.showFilmsList();
    }

}

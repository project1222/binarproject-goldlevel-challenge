package org.binar.ticket_reservation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.binar.ticket_reservation.dto.ScheduleDto;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.Schedules;
import org.binar.ticket_reservation.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@Tag(name = "Schedule", description = "API for processing with Schedule entity")
@RestController
@RequestMapping("/api")
public class ScheduleController {

    @Autowired
    @NonNull
    private ScheduleService scheduleService;

//            **********************************   Public Access   **********************************

    @Operation(summary = "Get List of Schedule by filmCode")
    @GetMapping("/public/schedule/{filmCode}")
    public ResponseEntity<List<Schedules>> getSchedulesByFilmCode(@PathVariable Films filmCode) {
        return scheduleService.getSchedulesByFilmCode(filmCode);
    }

//            **********************************   Admin Access   **********************************

    @Operation(summary = "Add a new schedule to Schedules Entity")
    @PostMapping("/admin/schedule")
    public ResponseEntity<Schedules> addSchedule(@RequestBody ScheduleDto scheduleDto) {
        Schedules schedules = new Schedules(scheduleDto);
        return scheduleService.saveSchedule(schedules.getFilmCode().getFilmCode(), schedules.getShowDate(), schedules.getStartTime(), schedules.getEndTime(), schedules.getTicketPrice());
    }

}

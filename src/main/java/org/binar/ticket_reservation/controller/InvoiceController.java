package org.binar.ticket_reservation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.binar.ticket_reservation.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
@Tag(name = "Invoice", description = "API for processing Generate PDF File")
@RestController
@RequestMapping("/api")
public class InvoiceController {

    @Autowired
    @NonNull
    private InvoiceService invoiceService;

//        **********************************   Customer Access   **********************************

    @Operation(summary = "Get PDF File of Ticket")
    @GetMapping("/customer/ticket")
    public void generateTicket(HttpServletResponse response,
                               @RequestParam String username, @RequestParam String filmName,
                               @RequestParam Character studioName, @RequestParam Integer seatNumber) throws JRException, IOException {
        invoiceService.generateInvoice(response, username, filmName, studioName, seatNumber);
    }

}

package org.binar.ticket_reservation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.binar.ticket_reservation.dto.UserDto;
import org.binar.ticket_reservation.model.Users;
import org.binar.ticket_reservation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@Tag(name = "Users", description = "API for processing with Users entity")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    @NonNull
    private UserService userService;

//        **********************************   Public Access   **********************************

    @Operation(summary = "Add a new user to Users Entity")
    @PostMapping("/public/user")
    public ResponseEntity<Users> addUser(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "A JSON object containing user information")
                                             @RequestBody UserDto userDto) {
        Users user = new Users(userDto);
        return userService.saveUser(user.getUsername(), user.getEmailAddress(), user.getPassword());
    }

//        **********************************   Customer Access   **********************************

    @Operation(summary = "Update a user information by it's ID/Primary Key")
    @PutMapping("/customer/user/{userId}")
    public ResponseEntity<Users> updateUser(@Parameter(description = "id of user to be searched")
                                                @PathVariable Integer userId,
                                            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "A JSON object containing user information")
                                                @RequestBody UserDto userDto) {
        Users user = new Users(userDto);
        return userService.updateUser(userId, user.getUsername(), user.getEmailAddress(), user.getPassword());
    }

    @Operation(summary = "Delete a user by it's ID/Primary Key")
    @DeleteMapping ("/customer/user/{userId}")
    public ResponseEntity<String> deleteUser(@Parameter(description = "id of user to be searched")
                                                 @PathVariable Integer userId) {
        return userService.deleteUserById(userId);
    }

//         **********************************   Admin Access(Bukan requirement challenge)   **********************************

    @Operation(summary = "Get all user in Users Entity")
    @GetMapping("/admin/users")
    public ResponseEntity<List<Users>> readAllUser() {
        return userService.showUsersList();
    }

}

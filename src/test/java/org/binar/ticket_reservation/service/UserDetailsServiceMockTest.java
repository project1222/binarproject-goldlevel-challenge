package org.binar.ticket_reservation.service;

import org.binar.ticket_reservation.model.UserDetailsImpl;
import org.binar.ticket_reservation.model.Users;
import org.binar.ticket_reservation.repository.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import static org.junit.jupiter.api.Assertions.*;

class UserDetailsServiceMockTest {

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private UserDetailsService userDetailsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.userDetailsService = new UserDetailsServiceImpl(usersRepository);
    }

    @Test
    void loadUserByUsername() {
        Users user1 = new Users("user1", "user1@email.com", "pass");
        user1.setUserId(1);
        Mockito.when(usersRepository.findByUsername(user1.getUsername())).thenReturn(user1);
        UserDetails expected = UserDetailsImpl.build(user1);

        Assertions.assertEquals(expected, userDetailsService.loadUserByUsername(user1.getUsername()));
    }
}
package org.binar.ticket_reservation.service;

import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.repository.FilmRepository;
import org.binar.ticket_reservation.repository.ScheduleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class FilmServiceMockTest {

    @Mock
    private FilmRepository filmRepository;

    @Mock
    private FilmService filmService;

    @Mock
    private ScheduleRepository scheduleRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.filmService = new FilmServiceImpl(filmRepository, scheduleRepository);
    }

    @Test
    void saveFilms() {
        Films film1 = new Films(1, "My Adventure", true);

        ResponseEntity<Films> expected = new ResponseEntity<>(filmRepository.save(film1), HttpStatus.CREATED);
        ResponseEntity<Films> saveUserResponse = filmService.saveFilms(film1.getFilmName(), film1.getIsBeingShown());

        Assertions.assertEquals(expected, saveUserResponse);
    }

    @Test
    void ShowAllCurrentlyShowingFilm() {
        List<Films> filmList = new ArrayList<>();
        filmList.add(new Films(1, "film1", true));
        filmList.add(new Films(2, "film2", true));

        Mockito.when(filmRepository.findFilmByIsBeingShownIsTrue()).thenReturn(filmList);
        ResponseEntity<List<Films>> expected = new ResponseEntity<>(filmList, HttpStatus.OK);

        Assertions.assertEquals(expected, filmService.showFilmsIsBeingShown());
    }

    @Test
    void ShowAllCurrentlyShowingFilmWhenFilmListIsEmpty() {
        ResponseEntity<List<Films>> expected = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        Mockito.when(filmRepository.findFilmByIsBeingShownIsTrue()).thenReturn(Collections.emptyList());

        Assertions.assertEquals(expected, filmService.showFilmsIsBeingShown());
    }

    @Test
    void updateFilmWhenTheFilmIsFound() {
        Films film1 = new Films(1, "My Adventure", true);
        Films updatedfilm1 = new Films(1, "My Adventure 2", true);

        ResponseEntity<Films> expected = new ResponseEntity<>(filmRepository.save(updatedfilm1), HttpStatus.OK);
        Mockito.when(filmRepository.findById(film1.getFilmCode())).thenReturn(Optional.of(film1));
        ResponseEntity<Films> filmMock = filmService.updateFilms(film1.getFilmCode(), updatedfilm1.getFilmName(), updatedfilm1.getIsBeingShown());

        Assertions.assertEquals(expected, filmMock);
    }

    @Test
    void updateFilmWhenFilmIsNotFound() {
        Integer filmCode = 2;
        ResponseEntity<Films> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Mockito.when(filmRepository.findById(filmCode)).thenReturn(Optional.of(new Films()));
        Assertions.assertEquals(expected, filmService.updateFilms(filmCode, "", false));
        Mockito.verify(filmRepository, Mockito.times(0)).save(new Films());
    }

    @Test
    void deleteFilmWhenTheFilmIsExist() {
        Integer filmCode = 2;
        ResponseEntity<String> expected = new ResponseEntity<>(HttpStatus.NO_CONTENT);

        Mockito.when(filmRepository.existsById(filmCode)).thenReturn(true);
        ResponseEntity<String> deleteFilmResponse = filmService.deleteFilmsById(filmCode);
        Mockito.verify(filmRepository, Mockito.times(1)).deleteById(filmCode);

        Assertions.assertEquals(expected, deleteFilmResponse);
    }

    @Test
    void deleteFilmWhenTheFilmIsNotExist() {
        Integer filmCode = 2;
        ResponseEntity<String> expected = new ResponseEntity<>("Film with filmCode " + filmCode + " doesn't exist", HttpStatus.INTERNAL_SERVER_ERROR);

        Mockito.when(filmRepository.existsById(filmCode)).thenReturn(false);
        ResponseEntity<String> deleteFilmResponse = filmService.deleteFilmsById(filmCode);
        Mockito.verify(filmRepository, Mockito.times(0)).deleteById(filmCode);

        Assertions.assertEquals(expected, deleteFilmResponse);
    }

    @Test
    void showAllFilm() {
        List<Films> filmList = new ArrayList<>();
        filmList.add(new Films());
        filmList.add(new Films());

        ResponseEntity<List<Films>> expected = new ResponseEntity<>(filmList, HttpStatus.OK);

        Mockito.when(filmRepository.findAll()).thenReturn(filmList);

        Assertions.assertEquals(expected, filmService.showFilmsList());
    }

    @Test
    void showAllFilmsWhenThereIsNoFilm() {
        ResponseEntity<List<Films>> expected = new ResponseEntity<>(HttpStatus.NO_CONTENT);

        Mockito.when(filmRepository.findAll()).thenReturn(Collections.emptyList());

        Assertions.assertEquals(expected, filmService.showFilmsList());
    }

    @Test
    void getFilmByFilmNameWhenTheFilmFound() {
        Films film1 = new Films(1, "My Adventure", true);

        ResponseEntity<Films> expected = new ResponseEntity<>(film1, HttpStatus.OK);

        Mockito.when(filmRepository.findByFilmName(film1.getFilmName())).thenReturn(film1);

        Assertions.assertEquals(expected, filmService.getFilmsByFilmName("My Adventure"));

    }

    @Test
    void getFilmByFilmNameWhenFilmIsNotExist() {
        ResponseEntity<Films> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Mockito.when(filmRepository.findByFilmName("NONE")).thenReturn(null);
        Assertions.assertEquals(expected, filmService.getFilmsByFilmName("NONE"));
    }
}
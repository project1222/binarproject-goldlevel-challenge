package org.binar.ticket_reservation.service;

import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.Schedules;
import org.binar.ticket_reservation.repository.FilmRepository;
import org.binar.ticket_reservation.repository.ScheduleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ScheduleServiceMockTest {

    @Mock
    private ScheduleRepository scheduleRepository;

    @Mock
    private FilmRepository filmRepository;

    @Mock
    private ScheduleService scheduleService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.scheduleService = new ScheduleServiceImpl(filmRepository, scheduleRepository);
    }

    @Test
    void addScheduleWhenFilmWithCertainIdFound() {
        Films film1 = new Films(1, "My Adventure", true);
        Schedules schedules1 = new Schedules(1, film1, "07/07/2022", "13:00", "15:00", 100);

        ResponseEntity<Schedules> expected = new ResponseEntity<>(scheduleRepository.save(schedules1), HttpStatus.CREATED);

        Mockito.when(filmRepository.findById(film1.getFilmCode())).thenReturn(Optional.of(film1));

        Assertions.assertEquals(expected, scheduleService.saveSchedule(film1.getFilmCode(), schedules1.getShowDate(), schedules1.getStartTime(), schedules1.getEndTime(), schedules1.getTicketPrice()));
    }

    @Test
    void addScheduleWhenFilmWithCertainIdNotFound() {

        Integer filmCode = 3;
        ResponseEntity<Schedules> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Mockito.when(filmRepository.findById(filmCode)).thenReturn(Optional.empty());

        Assertions.assertEquals(expected, scheduleService.saveSchedule(filmCode, "07/07/2022", "14:00", "16:00", 100));
    }

    @Test
    void getSchedulesByFilmCodeWhenTheScheduleIsNotExist() {
        ResponseEntity<List<Schedules>> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Films film1 = new Films(1, "Ice Age", true);

        Mockito.when(scheduleRepository.findByFilmCode(film1)).thenReturn(Collections.emptyList());

        Assertions.assertEquals(expected, scheduleService.getSchedulesByFilmCode(film1));
    }

    @Test
    void getScheduleByFilmCodeWhenTheScheduleIsExist() {

        Films film1 = new Films(1, "Ice Age", true);

        List<Schedules> schedulesList = new ArrayList<>();
        schedulesList.add(new Schedules());
        schedulesList.add(new Schedules());

        Mockito.when(scheduleRepository.findByFilmCode(film1)).thenReturn(schedulesList);

        ResponseEntity<List<Schedules>> expected = new ResponseEntity<>(schedulesList, HttpStatus.OK);

        Assertions.assertEquals(expected, scheduleService.getSchedulesByFilmCode(film1));
    }
}
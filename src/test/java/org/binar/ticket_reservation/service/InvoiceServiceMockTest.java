package org.binar.ticket_reservation.service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import org.apache.catalina.connector.Response;
import org.binar.ticket_reservation.model.*;
import org.binar.ticket_reservation.repository.FilmRepository;
import org.binar.ticket_reservation.repository.ScheduleRepository;
import org.binar.ticket_reservation.repository.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class InvoiceServiceMockTest {

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private FilmRepository filmRepository;

    @Mock
    private ScheduleRepository scheduleRepository;

    @Mock
    private UserService userService;

    @Mock
    private FilmService filmService;

    @Mock
    private InvoiceService invoiceService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.invoiceService = new InvoiceServiceImpl(usersRepository, filmRepository, scheduleRepository, userService, filmService);
    }

    @Test
    void generateInvoice() throws JRException, IOException {
        Users user1 = new Users("user1", "user1@mail.com", "user1");
        Films film1 = new Films(1, "My Adventure", true);
        Schedules schedules1 = new Schedules();
        schedules1.setFilmCode(film1);
        schedules1.setShowDate("15/04/2022");
        schedules1.setStartTime("13:00");
        schedules1.setEndTime("15:00");
        schedules1.setTicketPrice(100);
        List<Schedules> schedulesList = new ArrayList<>();
        schedulesList.add(schedules1);
        SeatId seatId1 = new SeatId('A', 13);
        Seats seat1 = new Seats(seatId1);

        Mockito.when(userService.getUsersByUsername(user1.getUsername())).thenReturn(new ResponseEntity<>(user1, HttpStatus.OK));
        Mockito.when(filmService.getFilmsByFilmName(film1.getFilmName())).thenReturn(new ResponseEntity<>(film1, HttpStatus.OK));
        Mockito.when(scheduleRepository.findByFilmCode(film1)).thenReturn(schedulesList);
        Mockito.when(userService.addSeat(seat1.getSeatId().getStudioName(), seatId1.getSeatNumber())).thenReturn(new ResponseEntity<>(seat1, HttpStatus.CREATED));

        Assertions.assertEquals(user1, userService.getUsersByUsername(user1.getUsername()).getBody());
        Assertions.assertEquals(film1, filmService.getFilmsByFilmName(film1.getFilmName()).getBody());
        Assertions.assertEquals(schedules1, scheduleRepository.findByFilmCode(film1).get(0));
        Assertions.assertEquals(seat1, userService.addSeat(seat1.getSeatId().getStudioName(), seatId1.getSeatNumber()).getBody());

        invoiceService.generateInvoice(new MockHttpServletResponse(), user1.getUsername(), film1.getFilmName(), seat1.getSeatId().getStudioName(), seatId1.getSeatNumber());

    }
}
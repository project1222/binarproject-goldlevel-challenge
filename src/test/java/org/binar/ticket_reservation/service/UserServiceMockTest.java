package org.binar.ticket_reservation.service;

import org.binar.ticket_reservation.model.SeatId;
import org.binar.ticket_reservation.model.Seats;
import org.binar.ticket_reservation.model.Users;
import org.binar.ticket_reservation.repository.SeatRepository;
import org.binar.ticket_reservation.repository.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UserServiceMockTest {

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private SeatRepository seatRepository;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.userService = new UserServiceImpl(usersRepository, seatRepository);
    }

    @Test
    void getAllUser() {
        Users user1 = new Users();
        Users user2 = new Users();

        List<Users> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);

        ResponseEntity<List<Users>> expected = new ResponseEntity<>(userList, HttpStatus.OK);

        Mockito.when(usersRepository.findAll()).thenReturn(userList);
        ResponseEntity<List<Users>> userListResponseEntity = userService.showUsersList();

        Assertions.assertEquals(expected, userListResponseEntity);
    }

    @Test
    void getAllUserWhenUserListIsEmpty() {
        ResponseEntity<List<Users>> expected = new ResponseEntity<>(HttpStatus.NO_CONTENT);

        Mockito.when(usersRepository.findAll()).thenReturn(Collections.emptyList());
        ResponseEntity<List<Users>> userList = userService.showUsersList();

        Assertions.assertEquals(expected, userList);
    }

    @Test
    void deleteUserWhenUserWithCertainIdIsExist() {
        Integer userId = 1;
        ResponseEntity<String> expected = new ResponseEntity<>(HttpStatus.NO_CONTENT);

        Mockito.when(usersRepository.existsByUserId(userId)).thenReturn(true);
        ResponseEntity<String> deleteUserResponse = userService.deleteUserById(userId);
        Mockito.verify(usersRepository, Mockito.times(1)).deleteById(userId);

        Assertions.assertEquals(expected, deleteUserResponse);
    }

    @Test
    void deleteUserWhenUserWithCertainIdIsNotExist() {
        Integer userId = 1;
        ResponseEntity<String> expected = new ResponseEntity<>("user with userId " + userId + " doesn't exist", HttpStatus.INTERNAL_SERVER_ERROR);

        Mockito.when(usersRepository.existsByUserId(userId)).thenReturn(false);
        ResponseEntity<String> deleteUserResponse = userService.deleteUserById(userId);
        Mockito.verify(usersRepository, Mockito.times(0)).deleteById(userId);

        Assertions.assertEquals(expected, deleteUserResponse);
    }

    @Test
    void addUser() {
        Users user1 = new Users("cust1", "cust1@gmail.com", "cust1");
        ResponseEntity<Users> expected = new ResponseEntity<>(usersRepository.save(user1), HttpStatus.CREATED);

        ResponseEntity<Users> saveUserResponse = userService.saveUser(user1.getUsername(), user1.getEmailAddress(), user1.getPassword());

        Assertions.assertEquals(expected, saveUserResponse);
    }

    @Test
    void findUserByUsernameSuccess() {
        Users user1 = new Users("cust1", "cust1@gmail.com", "cust1");
        user1.setUserId(1);

        ResponseEntity<Users> expected = new ResponseEntity<>(user1, HttpStatus.OK);

        Mockito.when(usersRepository.findByUsername(user1.getUsername())).thenReturn(user1);
        ResponseEntity<Users> userMock = userService.getUsersByUsername(user1.getUsername());
        Mockito.verify(usersRepository, Mockito.times(1)).findByUsername(user1.getUsername());

        Assertions.assertEquals(expected, userMock);
    }

    @Test
    void findByUsernameFailed() {
        ResponseEntity<Users> expected = new ResponseEntity<>(HttpStatus.OK);

        Mockito.when(usersRepository.findByUsername("NONE")).thenReturn(null);
        ResponseEntity<Users> userMock = userService.getUsersByUsername("NONE");

        Assertions.assertNull(userMock.getBody());
        Assertions.assertEquals(expected, userMock);
    }

    @Test
    void addSeat() {
        Seats seat1 = new Seats(new SeatId('A', 12));

        ResponseEntity<Seats> seatMock = userService.addSeat(seat1.getSeatId().getStudioName(), seat1.getSeatId().getSeatNumber());

        Assertions.assertSame(seatRepository.save(seat1), seatMock.getBody());
        Assertions.assertSame(HttpStatus.CREATED, seatMock.getStatusCode());
    }

    @Test
    void updateUserWhenUserIsExist() {
        Users user1 = new Users("cust1", "cust1@gmail.com", "cust1");
        user1.setUserId(1);

        ResponseEntity<Users> expected = new ResponseEntity<>(usersRepository.save(user1), HttpStatus.OK);

        Mockito.when(usersRepository.findById(user1.getUserId())).thenReturn(Optional.of(user1));
        ResponseEntity<Users> userMock = userService.updateUser(user1.getUserId(), user1.getUsername(), user1.getEmailAddress(), user1.getPassword());

        Assertions.assertEquals(expected, userMock);
    }

    @Test
    void updateUserWhenUserIsNotExist() {
        Integer userId = 1;

        ResponseEntity<Users> expected = new ResponseEntity<>( HttpStatus.NOT_FOUND);

        Mockito.when(usersRepository.findById(userId)).thenReturn(Optional.of(new Users()));

        Assertions.assertEquals(expected, userService.updateUser(userId, "username", "email Addres", "password"));
    }

}

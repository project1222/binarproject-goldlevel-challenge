package org.binar.ticket_reservation.controller;

import org.binar.ticket_reservation.dto.UserDto;
import org.binar.ticket_reservation.model.Users;
import org.binar.ticket_reservation.service.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserTest {

    @Mock
    private UserController userController;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.userController = new UserController(userService);
    }

    @Test
    @DisplayName("Menambah User baru")
    void testSaveUser() {
        UserDto userDto = new UserDto();
        ResponseEntity<Users> expected = userService.saveUser(userDto.getUsername(), userDto.getEmailAddress(), userDto.getPassword());
        ResponseEntity<Users> addUserResponse = userController.addUser(userDto);
        Assertions.assertEquals(expected, addUserResponse);
    }

    @Test
    @DisplayName("Menampilkan seluruh user")
    void showUsersList() {
        ResponseEntity<List<Users>> expected = userService.showUsersList();
        ResponseEntity<List<Users>> userListResponse = userController.readAllUser();
        Assertions.assertEquals(expected, userListResponse);
    }

    @Test
    @DisplayName("Mengupdate User")
    void updateUser() {
        UserDto userDto = new UserDto();
        ResponseEntity<Users> expected = userService.updateUser(1, userDto.getUsername(), userDto.getEmailAddress(), userDto.getPassword());
        ResponseEntity<Users> updateUser = userController.updateUser(1, userDto);
        Assertions.assertEquals(expected, updateUser);
    }

    @Test
    @DisplayName("Menghapus User berdasarkan userId")
    void deleteUserByIdSuccess() {
        Integer id = 1;
        ResponseEntity<String> expected = userService.deleteUserById(id);
        ResponseEntity<String> stringResponseEntity = userController.deleteUser(id);
        Assertions.assertEquals(expected, stringResponseEntity);
    }
//
//    @Test
//    @Disabled
//    void deleteUserByIdFail() {
//        Integer id = 100;
//        ResponseEntity<String> reality = userController.deleteUser(id);
//        Assertions.assertEquals(reality, new ResponseEntity<>("user with userId " + id + " doesn't exist", HttpStatus.INTERNAL_SERVER_ERROR));
//    }

}
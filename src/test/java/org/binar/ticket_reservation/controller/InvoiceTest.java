package org.binar.ticket_reservation.controller;

import net.sf.jasperreports.engine.JRException;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.SeatId;
import org.binar.ticket_reservation.model.Seats;
import org.binar.ticket_reservation.model.Users;
import org.binar.ticket_reservation.service.InvoiceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ExtendWith(MockitoExtension.class)
class InvoiceTest {

    @Mock
    private InvoiceController invoiceController;

    @Mock
    private InvoiceService invoiceService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.invoiceController = new InvoiceController(invoiceService);
    }

    @Test
    void generateTicketTest() throws JRException, IOException {
        HttpServletResponse httpServletResponse = new MockHttpServletResponse();
        Films film1 = new Films("My Adventure", true);
        Users user1 = new Users("user1", "user1@gmail.com", "mypass");
        Seats seat1 = new Seats(new SeatId('A', 12));
        invoiceController.generateTicket(httpServletResponse, user1.getUsername(), film1.getFilmName(), seat1.getSeatId().getStudioName(), seat1.getSeatId().getSeatNumber());
        Mockito.verify(invoiceService, Mockito.times(1)).generateInvoice(httpServletResponse, user1.getUsername(), film1.getFilmName(), seat1.getSeatId().getStudioName(), seat1.getSeatId().getSeatNumber());
    }
}

package org.binar.ticket_reservation.controller;

import org.binar.ticket_reservation.dto.FilmDto;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.service.FilmService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class FilmTest {

    @Mock
    private FilmController filmController;

    @Mock
    private FilmService filmService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.filmController = new FilmController(filmService);
    }


    @Test
    @DisplayName("Menambah film baru")
    void addFilm() {
        FilmDto filmDto = new FilmDto();
        ResponseEntity<Films> expected = filmService.saveFilms(filmDto.getFilmName(), filmDto.getBeingShown());
        ResponseEntity<Films> addFilmResponse = filmController.addFilm(filmDto);

        Assertions.assertEquals(expected, addFilmResponse);
    }

    @Test
    @DisplayName("Mengupdate Film")
    void testUpdateFilm() {
        Integer filmCode = 2;
        FilmDto filmDto = new FilmDto();
        ResponseEntity<Films> expected = filmService.updateFilms(filmCode, filmDto.getFilmName(), filmDto.getBeingShown());
        ResponseEntity<Films> updateFilmResponse = filmController.updateFilm(filmCode, filmDto);
        Assertions.assertEquals(expected, updateFilmResponse);

    }

    @Test
    @DisplayName("Membaca semua film")
    void testPrintReadFilm() {
        ResponseEntity<List<Films>> expected = filmService.showFilmsList();
        ResponseEntity<List<Films>> readAllFilmResponse = filmController.readAllFilm();
        Assertions.assertEquals(expected, readAllFilmResponse);
    }

    @Test
    @DisplayName("Menghapus Film berdasarkan filmCode")
    void testDeleteFilm() {
        Integer filmCode = 1;
        ResponseEntity<String> expected = filmService.deleteFilmsById(filmCode);
        ResponseEntity<String> deleteFilmResponse = filmController.deleteFilm(filmCode);
        Assertions.assertEquals(expected, deleteFilmResponse);
    }

    @Test
    @DisplayName("Membaca Film yang sedang tayang")
    void testFilmIsBeingShownIsTrue() {
        ResponseEntity<List<Films>> expected = filmService.showFilmsIsBeingShown();
        ResponseEntity<List<Films>> filmListResponse = filmController.readFilmIsBeingShown();
        Assertions.assertEquals(expected, filmListResponse);
    }

}

package org.binar.ticket_reservation.controller;

import org.binar.ticket_reservation.dto.ScheduleDto;
import org.binar.ticket_reservation.model.Films;
import org.binar.ticket_reservation.model.Schedules;
import org.binar.ticket_reservation.service.ScheduleService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class ScheduleTest {

    @Mock
    private ScheduleController scheduleController;

    @Mock
    private ScheduleService scheduleService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.scheduleController = new ScheduleController(scheduleService);
    }

    @Test
    @DisplayName("Menampilkan jadwal dari film tertentu")
    void readSchedule() {
        Films film = new Films(1);
        ResponseEntity<List<Schedules>> expected = scheduleService.getSchedulesByFilmCode(film);
        ResponseEntity<List<Schedules>> schedules = scheduleController.getSchedulesByFilmCode(film);
        Assertions.assertEquals(expected, schedules);
    }

    @Test
    void addScheduleTest() {
        ScheduleDto scheduleDto = new ScheduleDto(new Films(1), "19/01/2022", "14:00", "16:00", 100);
        ResponseEntity<Schedules> expected = scheduleService.saveSchedule(scheduleDto.getFilmCode().getFilmCode(), scheduleDto.getShowDate(), scheduleDto.getStartTime(), scheduleDto.getEndTime(), scheduleDto.getTicketPrice());
        ResponseEntity<Schedules> addSchedule = scheduleController.addSchedule(scheduleDto);
        Assertions.assertEquals(expected, addSchedule);
    }
}